<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Socket;

/**
 * Description of Read
 *
 * @author lucyann
 */
class Read
{

    protected $useCrypt = false;
    protected $chave    = null;

    /**
     *
     * @var \LENON\Model\CryptDecrypt
     */
    protected $cryptClass = null;

    private function getMsg($buf)
    {
        if ($this->useCrypt && (is_object($this->cryptClass)) && (!empty($this->chave))) {
            return trim($this->cryptClass->decrypt($buf, $this->chave));
        }
        else {
            return trim($buf);
        }
    }

    //put your code here
    public function listen(&$msgsock)
    {
        // verifica enter 
        do {
            if (!$buf = socket_read($msgsock, 2048)) {
                throw new \Exception("socket_read() failed: reason: " . socket_strerror(socket_last_error($msgsock)));
            }
            $msg = $this->getMsg($buf);
            
            
            // se nao for enter sai
            if (!empty($msg)) {
                break;
            }
        }
        while (true);
        
        // retorna msg        
        return $msg;
    }

    public function getUseCrypt()
    {
        return $this->useCrypt;
    }

    public function setUseCrypt($useCrypt)
    {
        $this->useCrypt = $useCrypt;
        return $this;
    }

    public function getChave()
    {
        return $this->chave;
    }

    public function getCryptClass()
    {
        return $this->cryptClass;
    }

    public function setChave($chave)
    {
        $this->chave = $chave;
        return $this;
    }

    public function setCryptClass(\LENON\Model\CryptDecrypt $cryptClass)
    {
        $this->cryptClass = $cryptClass;
        return $this;
    }

}
