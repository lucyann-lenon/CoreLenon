<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Socket;

use LENON\Socket\Read;

/**
 * Description of Send
 *
 * @author lucyann
 */
class Send extends Read
{

    private function getMsg($msg)
    {

        if ($this->useCrypt && (is_object($this->cryptClass)) && (!empty($this->chave))) {
            return trim($this->cryptClass->encrypt($msg, $this->chave));
        }
        else {
            return $msg;
        }
    }

    //put your code here
    public function sendMsg(&$socket, $msg)
    {
        if (!socket_write($socket, $this->getMsg($msg), strlen($this->getMsg($msg)))) {
            throw new \Exception("socket_read() failed: reason: " . socket_strerror(socket_last_error($socket)));
        }
    }

}
