<?php

namespace LENON\Socket;

use Exception;

class Server
{

    protected $socket;

    /**
     *
     * @var domain|ip domain or ip address from this socket
     */
    protected $address = null;

    /**
     * @var  int port 0-65535
     */
    protected $port = 60000;

    public function getAddress()
    {
        return $this->address;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setAddress($address)
    {
        if (!empty($address)) {
            $this->address = $address;
        }

        return $this;
    }

    public function setPort($port)
    {
        if (!empty($port)) {
            $this->port = $port;
        }

        return $this;
    }

    public function __construct($address = Null, $port = Null)
    {
        $this->setAddress($address);
        $this->setPort($port);
    }

    public function create()
    {
        // abre o socket 
        if (!$this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) {
            throw new Exception("socket_create() failed: reason: " . socket_strerror(socket_last_error()));
        }

        // executa o bind 
        $this->createBindSocket();

        // coloca o socket para scutar a porta 
        $this->createListenSocket();

        return $this->socket ;
    }

    /**
     * cria o bind socket
     */
    private function createBindSocket()
    {
        if (!socket_bind($this->socket, $this->address, $this->port)) {
            throw new Exception("socket_bind() failed: reason: " . socket_strerror(socket_last_error($this->socket)));
        }
    }

    /**
     * 
     * @throws Exception error if not listem 
     */
    private function createListenSocket()
    {
        if (!socket_listen($this->socket)) {
            throw new Exception("socket_listen() failed: reason: " . socket_strerror(socket_last_error($this->socket)));
        }
    }

    /**
     * 
     * @return type
     * @throws \Exception
     */
    public function acceptSocket()
    {


        if (!$msgsock = socket_accept($this->socket)) {
            throw new \Exception("socket_accept() failed: reason: " . socket_strerror(socket_last_error($this->socket)));
        }
        return $msgsock;
    }

    public function close(&$socket)
    {
        // close socket
        socket_close($socket);
    }

    public function getClientIpAddress(&$socket)
    {

        socket_getpeername($socket, $ipaddr);

        return $ipaddr;
    }

    public function getSocket()
    {
        return $this->socket;
    }

    public function setSocket(&$socket)
    {
        $this->socket = $socket;
        return $this;
    }


}
