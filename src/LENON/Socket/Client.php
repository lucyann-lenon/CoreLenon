<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Socket;

use LENON\Socket\Server;
use \Exception ;
/**
 * Description of Client
 *
 * @author lucyann
 */
class Client extends Server
{

    public function __construct($address = Null, $port = Null)
    {
        parent::__construct($address, $port);
    }

    public function create()
    {
        // cria o socket
        if (!$this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) {
            throw new Exception("socket_create() failed: reason: " . socket_strerror(socket_last_error()));
        }
        return $this->connect() ;
    }

    private function connect()
    {
        // cria o socket
        if (!$socketConn = socket_connect($this->socket, $this->address, $this->port)) {
            throw new Exception("socket_create() failed: reason: " . socket_strerror(socket_last_error()));
        }
        
        return $socketConn;
    }

}
