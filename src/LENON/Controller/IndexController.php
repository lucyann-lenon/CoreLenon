<?php

namespace LENON\Controller;

use Zend\View\Model\ViewModel;
use Gregwar\Captcha\CaptchaBuilder;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\AuthenticationService;
use LENON\Model as LENONModel; 

class IndexController extends AbstractActionController {

    public function indexAction()
    {
        return new ViewModel();
    }

    public function forbiddenAction()
    {

        $auth = new AuthenticationService();
        if ($auth->hasIdentity())
        {
            $dadosAuth = $auth->getIdentity();
            $em        = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $user      = $em->getRepository('Admin\Entity\Usuario')->findForAcl($dadosAuth['login']);

            if (empty($dadosAuth))
            {
                $auth->clearIdentity();
                return $this->redirect()->toUrl('/admin/index/auth');
            }
        }
        echo '<h1>estou squi</h1>';
        return new ViewModel();
    }

    public function noResourceAction()
    {
        echo '<h1>estou ssssss</h1>';
        return new ViewModel();
    }

    public function captchaAction()
    {

        $session = new \Zend\Session\Container('captcha' . $this->params()->fromRoute('id'));

        $builder          = new CaptchaBuilder;
        $builder->build();
        header('Content-type: image/jpeg');
        $builder->output();
        $session->captcha = $builder->getPhrase();

        // Turn off the layout, i.e. only render the view script.
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel;
    }

}
