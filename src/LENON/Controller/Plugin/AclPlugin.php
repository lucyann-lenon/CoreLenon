<?php

namespace LENON\Controller\Plugin;

use LENON\Model\IncludeJs,
    LENON\Model\LogFile,
    Admin\Entity\Perfil,
    Zend\Mvc\Controller\Plugin\AbstractPlugin,
    Zend\Mvc\MvcEvent,
    Zend\Permissions\Acl\Acl as Acl,
    Zend\Permissions\Acl\Role\GenericRole as Role,
    Zend\Permissions\Acl\Resource\GenericResource as Resource;

class AclPlugin extends AbstractPlugin
{

    //put your code here
    protected $sesscontainer;

    /**
     * 
     * @param \LENON\Controller\AbstractController $controller
     * @param \Zend\Authentication\AuthenticationService $auth
     * @return string
     */
    private function getAtualRole($controller, $auth)
    {
        if (!$auth->hasIdentity()) {
            return 'visitante';
        }
        else {
            $usuario = $auth->getIdentity();
            if (!($usuario instanceof \Admin\Entity\Usuario)) {
                if ((is_array($usuario) && (count($usuario) <= 0)) || empty($usuario)) {
                    $auth->clearIdentity();
                    return 'visitante';
                }
                $em   = $controller->getServiceLocator()->get('Doctrine\ORM\EntityManager');
                $user = $em->getRepository('Admin\Entity\Usuario')->findForAcl($usuario['login']);
                return strtolower($user['perfil']);
            }
            else {
                $perfil = $usuario->getPerfil();
                if ($perfil instanceof Perfil) {
                    return $usuario->getPerfil()->getNome();
                }
                else {
                    return strtolower($perfil);
                }
            }
        }
    }

    /**
     * @return Acl Description
     */
    private function montaAcl($controller)
    {

        // estancia das classes necessarias 
        $acl = new Acl();

        $em         = $controller->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
        $rowsPerfil = $em->getRepository('Admin\Entity\Perfil')->findBy(array(), array('id' => 'asc'));

        // adciona as roles 
        foreach ($rowsPerfil as $rowPerfil):
            $acl->addRole(new Role($rowPerfil->getNome()), $rowPerfil->getPerfilNome());
        endforeach;

        // add to resources 
        $confsResouces = include $_SERVER['PROJECT_ROOT'] . "/config/resource.config.php";

        foreach ($confsResouces as $rowResource) {
            $acl->addResource(new Resource($rowResource));
        }


        // monta as permissoes
        $confsAcl = include $_SERVER['PROJECT_ROOT'] . "/config/acl.config.php";

        foreach ($confsAcl as $rowAcl) {
            if ($rowAcl['tipo']) {
                $acl->allow($rowAcl['role'], $rowAcl['resource'], $rowAcl['privileges']);
            }
            else {
                $acl->deny($rowAcl['role'], $rowAcl['resource'], $rowAcl['privileges']);
            }
        }

        // permissao admin 
        $acl->allow('admin');

        //        \Zend\Debug\Debug::dump(true);
        return $acl;
    }

    public function doAuthorization(MvcEvent $e)
    {
//         verifica se o resource esta cadastrada 
        try {
            $controller = $e->getTarget();
            $auth       = $controller->getServiceLocator()
                    ->get('Zend\Authentication\AuthenticationService');

            $dados               = $controller->params()->fromRoute();
            $dados['controller'] = preg_replace('/^[\d\w]+\\\[\d\w]+\\\/i', '', strtolower($dados['controller']));


            //verifica o controlador é o de criação dos zf  
            if (($dados['controller'] === 'create')) {

                return false;
            }

            // monta o cahce
            $cache = $controller->getServiceLocator()->get('Cache');

            // verifica se a acl esta cacheado 
            if (!$acl = $cache->getItem('AclPermissao')) {
                $acl = $this->montaAcl($controller);
                $cache->addItem('AclPermissao', $acl);
            }

            // seta o caminho real 
            IncludeJs::setRouteName("/{$dados['module']}/{$dados['controller']}/{$dados['action']}");

            // verifica se tem permissão 
            if (!$acl->isAllowed($this->getAtualRole($controller, $auth), "{$dados['module']}:{$dados['controller']}", $dados['action'])) {
                if ($auth->hasIdentity()) {
                    //- assemble your own URL - this is just an example

                    $response = $e->getResponse();
                    $response->getHeaders()->addHeaderLine('Location', '/forbidden');
                    $response->setStatusCode(302);
                    $response->sendHeaders();
                    exit;
                }
                else {

                    $response = $e->getResponse();
                    $response->getHeaders()->addHeaderLine('Location', '/admin/index/auth');
                    $response->setStatusCode(302);
                    $response->sendHeaders();
                    exit;
                }
            }
        }
        catch (\Zend\Permissions\Acl\Exception\InvalidArgumentException $ex) {


            new LogFile(LogFile::CRIT, $ex->getMessage());
            echo $ex->getMessage();
            exit;
        }
        catch (\Exception $e) {
            new LogFile(LogFile::CRIT, $e->getMessage());
            echo $e->getMessage();
            exit;
        }
    }

}
