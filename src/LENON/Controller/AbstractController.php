<?php

namespace LENON\Controller;

use \Admin\Entity\Usuario,
    \Doctrine\ORM\EntityManager,
    \Exception,
    \LENON\Service\Log,
    \Zend\Authentication\AuthenticationService,
    \Zend\Cache\Storage\StorageInterface,
    \Zend\Db\Adapter\Adapter,
    \Zend\Mvc\Controller\AbstractActionController;

abstract class AbstractController extends AbstractActionController
{

    /**
     * 
     * @return EntityManager
     */
    protected function getEm()
    {
        $em = $this->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');
        return $em;
    }

    /**
     * 
     * @return Adapter
     */
    protected function getDefaultAdapter()
    {
        $adpter = $this->getServiceLocator()
                ->get('Zend\Db\Adapter\Adapter');

        return $adpter;
    }

    /**
     * @return Usuario
     */
    protected function getEntityUsuario()
    {
        $auth  = new AuthenticationService();
        $login = $auth->getIdentity();

        $em = $this->getEm();

        $repositoryLogin = $em->find('Admin\Entity\Usuario', $login['login']);

        return $repositoryLogin;
    }

    /**
     * 
     * @return  StorageInterface
     * @throws \Exception
     */
    protected function getCache()
    {
        if ($this->getServiceLocator()->has('Cache')) {
            // pega o serviço 
            $cache = $this->getServiceLocator()->get('Cache');
            // verifica se o serviço é valido
            if (!($cache instanceof StorageInterface)) {
                throw new Exception("Serviço de Cache configurado incorretamente !");
            }
            return $cache;
        }
        else {
            throw new Exception('Nenhum serviço de cache configurado ');
        }
    }

    /**
     * 
     * @return AuthenticationService
     */
    protected function getAuthenticationService()
    {
        return $this->getServiceLocator()
                        ->get('Zend\Authentication\AuthenticationService');
    }

    protected function log($msg, $type = 'info', $file = null, $line = null)
    {
        
    }
    /**
     * 
     * @return Log
     */
    protected function getLog()
    {
        return $this->getServiceLocator()->get('Log') ; 
    }

    /**
     * 
     * @param \Exception $excpt
     */
    protected function logException($excpt)
    {
        $this->log($excpt->getMessage(), 'err', $excpt->getFile(), $excpt->getLine());
    }

}
