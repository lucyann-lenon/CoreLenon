<?php

namespace LENON\Exception;

use \Exception;

class ValidateExceptionJson extends Exception
{

    public function trataMessage(array $msg)
    {
        $dados = array();

        foreach ($msg as $k => $rows):
            foreach ($rows as $linha) :
                $dados[$k][] = $linha;
            endforeach;
        endforeach;
        
        return json_encode($dados);
    }

    public function __construct($message = "", $code = 0,
                                Exception $previous = null)
    {


        parent::__construct($this->trataMessage($message), $code, $previous);
    }

}
