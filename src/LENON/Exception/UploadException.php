<?php
namespace LENON\Exception ; 
use Exception ;
class UploadException extends Exception 
{ 
    public function __construct($code) { 
        $message = $this->codeToMessage($code); 
        parent::__construct($message, $code); 
    } 

    private function codeToMessage($code) 
    { 
        switch ($code) { 
            case UPLOAD_ERR_INI_SIZE: 
                $message = "O arquivo enviado excede o tamanho maximo permitido pelas configurações do sistema";
                break; 
            case UPLOAD_ERR_FORM_SIZE: 
                $message = "O arquivo enviado excede o tamanho maximo permitido "; 
                break; 
            case UPLOAD_ERR_PARTIAL: 
                $message = "O arquivo foi apenas parcialmente enviado"; 
                break; 
            case UPLOAD_ERR_NO_FILE: 
                $message = "Nenhum arquivo foi enviado"; 
                break; 
            case UPLOAD_ERR_NO_TMP_DIR: 
                $message = "Sistema sem pasta temporaria "; 
                break; 
            case UPLOAD_ERR_CANT_WRITE: 
                $message = "Não foi possível escrever no disco "; 
                break; 
            case UPLOAD_ERR_EXTENSION: 
                $message = "Arquivo foi parado devido a extenção duvidosa "; 
                break; 

            default: 
                $message = "Unknown upload error "; 
                break; 
        } 
        return $message; 
    } 
} 