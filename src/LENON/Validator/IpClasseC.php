<?php

namespace LENON\Validator;

use Zend\Validator\AbstractValidator;

class IpClasseC extends AbstractValidator
{

    const IPINVLAIDO = '1';
    const IPJAEXIST  = '2';

    private $error = null;

    /**
     *
     * @var \Application\Entity\Parceiro  
     */
    private $parceiro = null;

    /**
     *
     * @var \Application\Entity\Repository\Lan 
     */
    private $lan                = null;
    
    private $idLan = null ; 
    
    protected $messageTemplates = array(
        self::IPINVLAIDO => "'%value%' este ip não pode ser usadado em uma rede interna!",
        self::IPJAEXIST  => "'%value%' este ip não pode ser usadado, pois está dentro da range de outra VLan!",
    );

    protected function error($messageKey, $value = null)
    {
        $this->error = $messageKey;
        return parent::error($messageKey, $value);
    }

    public function getError()
    {
        return $this->error;
    }

    public function getParceiro()
    {
        return $this->parceiro;
    }

    public function getMessageTemplates()
    {
        return $this->messageTemplates;
    }

    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }

    public function setParceiro(\Application\Entity\Parceiro $parceiro)
    {
        $this->parceiro = $parceiro;
        return $this;
    }

    public function setMessageTemplates($messageTemplates)
    {
        $this->messageTemplates = $messageTemplates;
        return $this;
    }

    public function getLan()
    {
        return $this->lan;
    }

    public function setLan(\Application\Entity\Repository\Lan $lan)
    {
        $this->lan = $lan;
        return $this;
    }

    public function existRange($ip)
    {
        // verifica se parceiro
        if (!$this->parceiro) {
            throw new \Exception("Por favor set o parceiro !");
        }

        // verifica existe a lan 
        if (!$this->lan) {
            throw new \Exception("Por favor set o Lan !");
        }


        $rows = $this->lan->findBy(array('parceiro' => $this->parceiro), array('id' => 'ASC'));

        foreach ($rows as $row) {
            if ($row->getId() != $this->idLan) {
                // pega as interfaces 
                $dadosInterface = array();
                exec("ipcalc {$row->getIp()} {$row->getMask()} ", $dadosInterface);

                preg_match('/(?P<minIp>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/', $dadosInterface['5'], $min);
                preg_match('/(?P<maxIp>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/', $dadosInterface['6'], $max);


                $ipNumMin = ip2long($min['minIp']);
                $ipNumMax = ip2long($max['maxIp']);
                $ipNum    = ip2long($ip);

                if (($ipNum >= $ipNumMin ) && ($ipNum <= $ipNumMax)) {
                    return true;
                }
            }
        }
    }

    public function isValid($value)
    {
        // pega o ip
        list($ip, $mask) = explode('/', $value);

        // dadps
        $this->value = $ip;
        $count       = trim(shell_exec("ipcalc {$ip} {$mask} | grep 'Private Internet' | wc -l"));


        // verica
        if (!$count) {

            $this->error(self::IPINVLAIDO);
            return false;
        }

        if ($this->existRange($ip)) {
            $this->error(self::IPJAEXIST);
            return false;
        }

        return true;
    }

    public function getMessageError()
    {

        return str_replace('%value%', $this->value, $this->messageTemplates[$this->error]);
    }
    public function getIdLan()
    {
        return $this->idLan;
    }

    public function setIdLan($idLan)
    {
        $this->idLan = $idLan;
        return $this;
    }


//put your code here
}
