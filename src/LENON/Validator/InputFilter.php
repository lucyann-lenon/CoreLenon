<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Validator;

use Zend\InputFilter\InputFilter as ZendInputFilter;
use Zend\InputFilter\InputFilterInterface;
use Doctrine\ORM\EntityManager;

class InputFilter extends ZendInputFilter implements InputFilterInterface
{

    /**
     *
     * @var EntityManager
     */
    protected $em = null;
    protected $add;

    //put your code here
    public function isValid()
    {
        $retorno = parent::isValid();

        $count = $this->getMessages();
        if (count($count) <= 0) {
            return true;
        }
        else {
            return $retorno;
        }
    }

   /**
    * 
    * @param \Zend\InputFilter\Input $input
    */
    public function checkAddInput($input)
    {
        if (($this->add == 'all') || (in_array($input->getName(), $this->add))) {
           
            $this->add($input) ;
        }
    }

    public function __construct($em, $add = 'all')
    {
        $this->em  = $em;
        $this->add = $add;
    }

}
