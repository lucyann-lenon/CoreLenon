<?php

namespace LENON\Form;

use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form as zendForm;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;

class DefaultForm extends zendForm {

    protected function montaFormFieldSet(array $array)
    {

        // passa pelo array
        foreach ($array as $k => $row)
        {
            if (count($row) > 0)
            {
                if (strlen($row['name']) > 0)
                {
                    // inicia o fieldset
                    $fieldSet = new Fieldset($row['name']);
                }
                else
                {
                    $fieldSet = &$this;
                }

                foreach ($row['elements'] as $rowElement)
                {

                    if ($rowElement['attr']['type'] == 'fieldset')
                    {
                        $this->montaFormFieldSet($rowElement);
                    }
                    else
                    {
                        // monta o elemento 
                        $element = new Element($rowElement['name']);
                        $element->setLabel($rowElement['label']);

                        if (count($rowElement['attrs']))
                        {
                            $element->setAttributes(
                                    $rowElement['attrs']
                            );
                        }

                        if (count($rowElement['options']) > 0)
                        {
                            $element->setOptions($rowElement['options']);
                        }

                        $fieldSet->add($element);
                    }
                }
                if (strlen($row['name']) > 0)
                {
                    // inicia o fieldset
                    $this->add($fieldSet);
                }
            }
        }
    }

    public function __construct($name = null, $options = array())
    {
     
        
        parent::__construct($name, $options);
        
    }

}
