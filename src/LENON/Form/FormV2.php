<?php

namespace LENON\Form;

use Zend\Form\Element;
use Zend\Form\Form as zendForm;

class FormV2 extends zendForm
{

    /**
     * 
     * @param string $name
     * @param string $label
     * @return \Zend\Form\Element\Text
     */
    protected function createElementText($name, $label)
    {


        $element = new Element\Text($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');

        return $element;
    }

        /**
     * 
     * @param string $name
     * @param string $label
     * @return \Zend\Form\Element\Hidden
     */
    protected function createElementHidden($name, $label=null)
    {
        $element = new Element\Hidden($name);

        return $element;
    }

    protected function createElementTextArea($name, $label, $rows = 5)
    {


        $element = new Element\Textarea($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');
        $element->setAttribute('rows', $rows);

        return $element;
    }

    /**
     * 
     * @param string $name
     * @param string $label
     * @return \Zend\Form\Element\Email
     */
    protected function createElementEmail($name, $label)
    {


        $element = new Element\Email($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');

        return $element;
    }

    /**
     * 
     * @param string $name
     * @param string $label
     * @return \Zend\Form\Element\Text
     */
    protected function createElementDate($name, $label)
    {


        $element = new Element\Date($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');

        return $element;
    }

    /**
     * 
     * @param string $name
     * @param string $label
     * @return \Zend\Form\Element\Password
     */
    protected function createElementPassword($name, $label)
    {


        $element = new Element\Password($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');

        return $element;
    }

    /**
     * 
     * @param string $name
     * @param string $label
     * @return \Zend\Form\Element\File
     */
    protected function createElementFile($name, $label)
    {

        $element = new Element\File($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');

        return $element;
    }

    protected function createElementSelect($name, $label, Array $valueOptions = array())
    {

        $element = new Element\Select($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');
        $element->setValueOptions($valueOptions);

        return $element;
    }

  

}
