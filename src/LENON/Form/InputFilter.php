<?php

namespace LENON\Form;

use Zend\InputFilter\InputFilter as ZendInputFilter;
use \Zend\InputFilter\InputFilterInterface;

class InputFilter extends ZendInputFilter implements InputFilterInterface
{

    public function __construct(array $campos)
    {
        foreach ($campos as $rowCampo):
            $this->add($rowCampo);
        endforeach;
    }

}
