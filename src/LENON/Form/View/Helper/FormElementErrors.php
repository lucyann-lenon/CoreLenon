<?php

namespace LENON\Form\View\Helper;

use Zend\Form\View\Helper\FormElementErrors as OriginalFormElementErrors;

class FormElementErrors extends OriginalFormElementErrors
{

    protected $messageCloseString = '</li></ul></div>';
    protected $messageOpenFormat = '<div class="alert alert-warning alert-dismissible form-msg-error" role="alert" > <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><ul%s><li class="error">';
    protected $messageSeparatorString = '</li><li class="error">';

}
