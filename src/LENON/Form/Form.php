<?php

namespace LENON\Form;

use Zend\Form\Element;
use Zend\Form\Form as zendForm;

class Form extends zendForm
{

    /**
     *
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $adapter = null;

    /**
     * 
     * @return \Zend\Db\Adapter\Adapter
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    public function setAdapter($adapter)
    {
        $this->adapter = $adapter;
        return $this;
    }

    /**
     *
     * @see array(
      'name'      => '',
      'class'     => '',
      'style'     => '',
      'legend'    => '',
      'campos'    => array(
      'campoName'
      ),
      'fieldsets' =>array();
     * 
     */
    protected $fildesets = array();

    /**
     *
     * @var type array(
      'type'       => 'Zend\Form\Element\Submit',
      'name'       => 'salvar',
      'options'    => array(
      'label' => ''
      ),
      'attributes' => array(
      'value' => 'Cadastrar'
      )
     */
    protected $campos = array();

    /**
     *
     * @var type ,
      'required'   => true,
      'filters'    => array(
      array('name' => 'StringTrim'),
      array('name' => 'StripTags'),
      ),
      'validators' => array(
      array('name' => 'LENON\Validator\CpfCnpj')
      )
     */
    protected $filters = array();

    public function __construct($name = null, $options = array(), $adapter = null)
    {
        $this->setAdapter($adapter);
        if (empty($name))
        {
            parent::__construct('Parceiros');
        }

        foreach ($this->campos as $row):
            if (count($row['attrs']) > 0)
            {
                // monta o elemento 
                $element = new Element($row['name']);

                $element->setLabel($row['label']);

                $element->setAttributes(
                        $row['attrs']
                );

                if (count($row['options']) > 0)
                {
                    $element->setOptions($row['options']);
                }

                $this->add($element);
            }
            else
            {
                $this->add($row);
            }
        endforeach;
    }

    /**
     * 
     * @param string $sql
     * @param string $key
     * @param string $value
     */
    protected function mountDoctrineSelectOptions($sql, $key, $value)
    {
        $result = $sql->execute();

        $selectData = array();

        foreach ($result as $res)
        {
            $selectData[$res[$key]] = $res[$value];
        }

        return $selectData;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function getCampos()
    {
        return $this->campos;
    }

    public function setFildesets($fildesets)
    {
        $this->fildesets = $fildesets;
    }

    public function setCampos($campos)
    {
        $this->campos = $campos;
    }

    public function getFildesets()
    {
        if (count($this->fildesets) <= 0)
        {
            return false;
        }

        return $this->fildesets;
    }

    //put your code here    
    public function montarForm($view)
    {
        echo $view->form()->openTag($this);

        if (!$filelds = $this->getFildesets())
        {

            echo $view->formCollection($this);
        }
        else
        {
            $this->montarFielSet($filelds, $view);
        }
        echo $view->form()->closeTag();
    }

    protected function montarFielSet($fields, $view)
    {

        foreach ($fields as $rowField):
            $fieldsetHtml = '<fieldset ';

            $fieldsetHtml.=(!empty($rowField['name']) ? " id=\"fieldset-{$rowField['name']}\"" : '');
            $fieldsetHtml.=(!empty($rowField['class']) ? " class=\"{$rowField['class']}\"" : '');
            $fieldsetHtml.=(!empty($rowField['style']) ? " style=\"{$rowField['style']}\" >" : '>');

            echo $fieldsetHtml;

            if (!empty($rowField['legend']))
            {
                echo "<legend>{$rowField['legend']}</legend>";
            }
            elseif (is_array($rowField['legend']) && (count($rowField['legend']) > 0))
            {
                $legend = '<legend ';
                $legend.=(!empty($rowField['legend']['name']) ? " id=\"legend-{$rowField['legend']['name']}\"" : '');
                $legend.=(!empty($rowField['legend']['class']) ? " class=\"{$rowField['legend']['class']}\"" : '');
                $legend.=(!empty($rowField['legend']['style']) ? " style=\"{$rowField['legend']['style']}\" >" : '>');
                echo $legend['legend']['text'];
                echo '</legend>';
            }

            foreach ($rowField['campos'] as $rowCampos):

                if (is_array($rowCampos))
                {
                    echo "<div class=\"{$rowCampos['class']}\">";
                    if (!empty($rowCampos['label']))
                    {
                        echo "<label>{$rowCampos['label']}</label>";
                    }

                    foreach ($rowCampos['campos'] as $rowCamposCampos)
                    {
                        if ($this->has($rowCamposCampos))
                        {
                            if ($rowCampos['disableLabel'])
                            {
                                $element = $this->get($rowCamposCampos);
                                echo $view->formElement($element);
                                echo $view->formElementErrors($element);
                            }
                            else
                            {
                                echo $view->formRow($this->get($rowCamposCampos));
                            }
                        }
                    }
                    echo "</div>";
                }
                elseif ($this->has($rowCampos))
                {
                    echo $view->formRow($this->get($rowCampos));
                }

            endforeach;

            if (count($rowField['fieldsets']) > 0)
            {
                $this->montarFielSet($rowField['fieldsets'], $view);
            }

            echo '</fieldset>';
        endforeach;
    }

    protected function createElementText($name, $label)
    {


        $element = new Element\Text($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');

        return $element;
    }

    protected function createElementPassword($name, $label)
    {


        $element = new Element\Password($name);
        $element->setLabel($label);
        $element->setAttribute('class', 'form-control ');

        return $element;
    }

}
