<?php

namespace LENON\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zend\ServiceManager\ServiceManager;
use Doctrine\ORM\Query;

class AbstractRepository extends EntityRepository
{

    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEm()
    {
        return $this->getEntityManager();
    }

    //put your code here

    public function getCache()
    {
        // diretorio do aquivo 
        $filename = __DIR__ . '/../../../../../../config/cache.config.php';

        // verifica se o arquivo existe 
        if (!file_exists($filename)) {
            \throwException('O arquivo de confituração do cache não existe');
        }

        // cria o cache 
        $cfg   = include $filename;
        $cache = \Zend\Cache\StorageFactory::factory($cfg);

        return $cache;
    }

    // faz o insert generico 
    public function insert($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        
        return $entity;
    }

    // faz o remove generico 
    public function remove($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * Counts how many users there are in the database
     *
     * @return int
     */
    public function countQuery($query)
    {

        $result = $query->getQuery()->getResult();

        return count($result);
    }

    /**
     * Returns a list of users
     *
     * @param int $offset           Offset
     * @param int $itemCountPerPage Max results
     *
     * @return array
     */
    public function getItemsPaginator($query, $offset, $itemCountPerPage)
    {
        $query->setFirstResult($offset)
                ->setMaxResults($itemCountPerPage);

        $result = $query->getQuery()->getResult(Query::HYDRATE_ARRAY);

        return $result;
    }

    public function getItems($offset, $itemCountPerPage)
    {
        $query = $this->queryGetIntems;


        return $this->getItemsPaginator($query, $offset, $itemCountPerPage);
    }

    /**
     * 
     * @param array $condicao
     * @param  \Doctrine\ORM\QueryBuilder $query
     * @return \Doctrine\ORM\QueryBuilder Description
     */
    protected function montarWhereDql($condicao, &$query)
    {
        $i     = 0;
        $count = 0;
        foreach ($condicao as $rowCondicao) {
            $qu = $query;

            // monta os dado or
            if (count($rowCondicao) <= 0) {
                $partial = $query;
            }
            if ($rowCondicao['union'] == 'OR') {
                $partial = $qu->expr()->orX();
            }
            else {
                $partial = $qu->expr()->andX();
            }

            foreach ($rowCondicao['condicoes'] as $rowWhere) {


                if (strtoupper($rowWhere['tipo']) === 'LIKE') {
                    $partial->add($qu->expr()->like($rowWhere['campo'], "?{$i}"));

                    $query->setParameter($i, "%{$rowWhere['value']}%");
                }
                elseif (($rowWhere['tipo'] === '==') || (strtoupper($rowWhere['tipo']) === 'EQ')) {

                    $partial->add($qu->expr()->eq($rowWhere['campo'], "?{$i}"));
                    $query->setParameter($i, $rowWhere['value']);
                }

                $i++;
            }
            if ($count > 0) {
                $query->andWhere($partial);
            }
            else {
                $query->where($partial);
            }$count ++;
        }
    }

    protected function findForPaginationBase($dql, $dqlCount, $limit, $start, $short, $condicao)
    {
        // normal
        $this->montarWhereDql($condicao, $dql);

        // where count
        $this->montarWhereDql($condicao, $dqlCount);


        // monta o sql 
        $dql->setMaxResults($limit)
                ->setFirstResult($start);

        $data  = $dql->getQuery()
                ->getArrayResult()
        ;
//
//        var_dump($dql->getQuery()->getSQL());
//        var_dump($data);
//        return array();
        $count = $dqlCount->getQuery()
                ->getOneOrNullResult();

        return array('total' => $count['total'], 'data' => $data);
    }

}
