<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Entity;

use Zend\Stdlib\Hydrator\ClassMethods;
use Doctrine\ORM\EntityManager;

abstract class Entity
{

    /**
     *
     * @var EntityManager 
     */
    protected $em;
    protected $toArray = false;

    public function __construct(Array $options = array())
    {
        (new ClassMethods)->hydrate($options, $this);
    }

    public function toArray()
    {
        // evita que retorne objetos 
        $this->toArray = true;

        $dados = (new ClassMethods)->extract($this);

        // evita que n]ao retorne objetos
        $this->toArray = false;
        return$dados;
    }

    /**
     * 
     * @param array $options
     * @param EntityManager $em
     */
    public function hydrate(Array $options = array(),$em =null )
    {
        $this->em = $em ; 
        (new ClassMethods)->hydrate($options, $this);
    }

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * Convert the object to an array.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * Populate from an array.
     *
     * @param array $data
     */
    public function exchangeArray($data = array())
    {
        $this->hydrate($data);
    }

    public function getEm()
    {
        return $this->em;
    }

    public function getToArray()
    {
        return $this->toArray;
    }

    public function setEm(EntityManager $em)
    {
        $this->em = $em;
        return $this;
    }

    public function setToArray($toArray)
    {
        $this->toArray = $toArray;
        return $this;
    }

    public function setReference($key, $entity)
    {
        if (($this->em instanceof EntityManager ) && (!is_object($key))) {
            
            return $this->em->getReference($entity, $key);
        }
        else {
            return $key;
        }
    }

    public function setDateTime($date)
    {
        if (!($date instanceof \DateTime) && !(is_object($date))) {
            return new \DateTime($date);
        }
        else {
            return $date;
        }
    }

}
