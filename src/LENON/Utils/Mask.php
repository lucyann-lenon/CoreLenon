<?php

namespace LENON\Utils;

class Mask
{

    /**
     * example Cep  val = '36950000' , mask ='#####-###' return 35950-000
     * @param string $val
     * @param string $mask
     * @return strin
     */
    public static function mask($val, $mask)
    {
        $maskared = '';
        $k        = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /**
     * example Cep  val = '35950-000' , mask ='#####-###' return 35950000
     * @param string $val
     * @param string $mask
     * @return strin
     */
    public static function unMask($val, $mask)
    {
        $path = "/[" . str_replace("#", '', $mask) . "]/";

        return preg_replace($path, '', $val);
    }

    public static function maskCpfCnpj($val)
    {
        $val = self::unMaskCpfCnpj($val);

        if (strlen($val) > 11) {
            return self::mask($val, "##.###.###/####-##");
        }
        else {
            return self::mask($val, "###.###.###-##");
        }
    }

    public static function unMaskCpfCnpj($val)
    {
        return preg_replace('/[^0-9]/', '', $val);
    }

}
