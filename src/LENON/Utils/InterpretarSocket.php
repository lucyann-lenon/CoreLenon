<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Utils;

/**
 * Description of InterpreteSocket
 *
 * @author lucyann
 */
class InterpretarSocket
{

    /**
     *
     * @var  \AutoDb\Comandos\Comandos
     */
    private $comandClass = null;

    /**
     *
     * @var \LENON\Socket\Server 
     */
    private $socketClass = null;

    public function __construct($comandos, &$socket)
    {
        $this->comandClass = $comandos;
        $this->socketClass = $socket;
        return $this;
    }

    /**
     * 
     * @param string $cmd
     * @return bool
     */
    public function retornaBool($cmd)
    {
        // remove espaços e coloca os caracteres em minusculos 
        $cmd  = trim(strtolower($cmd));
        $bool = array(
            's',
            'sim',
            'y',
            'yes'
        );

        return in_array($cmd, $bool) ? true : false;
    }

    /**
     * 
     * @param string $cmd
     * @return bool
     */
    public function checkExit($cmd)
    {
        // remove espaços e coloca os caracteres em minusculos 
        $cmd = trim(strtolower($cmd));

        // verfica se vai ser retirado
        $exit = array('exit', 'quit');


        return in_array($cmd, $exit) ? true : false;
    }

    public function executeCmd($cmd, $msgSocket)
    {
        $this->comandClass->setOptions(array('ip' => $this->socketClass->getClientIpAddress($msgSocket)));
        $retorno = $this->comandClass->exec($cmd);
        if (empty($retorno)) {
            return "ok !\n";
        } else {
            return $retorno;
        }
    }

}
