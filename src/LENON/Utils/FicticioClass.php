<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Utils;

use \ArrayIterator;

/**
 * Description of FicticioClass
 *
 * @author lucyann
 */
class FicticioClass
{

    public function __construct($array = 'array()')
    {
        foreach ($array as $key => $value) {

            $this->$key = $value;
        }
    }

    public function __call($name, $arguments)
    {
        $maths = null;
        if (preg_match('/^get[A-Z]/', $name)) {
            $nome = lcfirst(str_replace("get", '', $name));
            return $this->$nome;
        }
    }

    public function get($name)
    {

        return $this->$name;
    }

}
