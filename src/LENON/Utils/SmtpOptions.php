<?php

namespace LENON\Utils;

use Zend\Mail\Transport\SmtpOptions as ZendSmtpOptions;

class SmtpOptions extends ZendSmtpOptions
{

    public function setAllowSelfSigned()
    {
        stream_context_create(array(
            "ssl" => array(
                "verify_peer"       => false,
                'verify_peer_name'  => false,
               // "allow_self_signed" => true,
            )
        ));
    }

}
