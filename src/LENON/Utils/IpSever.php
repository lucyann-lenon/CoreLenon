<?php

namespace LENON\Utils;

class IpServer
{

    public static function getIpServer($ip)
    {
        $ip = ip2long($ip);

        // pega os ips da interface 
        $interfaces = array();
        exec("ip addr show |  awk -F 'inet' '{print $2}' | grep 'global' | awk '{print $1}'", $interfaces);

        foreach ($interfaces as $row):

            list($ip2, $mask) = explode("/", $row);

            $dadosInterface = array();
            exec("ipcalc {$ip2} {$mask} ", $dadosInterface);

            preg_match('/(?P<minIp>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/', $dadosInterface['5'], $min);
            preg_match('/(?P<maxIp>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/', $dadosInterface['6'], $max);

            $min = ip2long($min['minIp']);
            $max = ip2long($max['maxIp']);

            // pega a conexao de ip
            if (($ip >= $min) && ($ip <= $max)) {
                return $ip2;
            }

        endforeach;
    }

}
