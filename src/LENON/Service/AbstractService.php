<?php

namespace LENON\Service;

use \Doctrine\ORM\EntityManager;

abstract class AbstractService
{

    /**
     *
     * @var Log 
     */
    protected $log;

    /**
     *
     * @var EntityManager 
     */
    protected $em;

    /**
     * @return EntityManager  
     */
    public function getEm()
    {
        return $this->em;
    }

    public function setEm(EntityManager $em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * 
     * @return Log
     */
    public function getLog()
    {
        return $this->log;
    }

    public function setLog(Log $log)
    {
        $this->log = $log;
        return $this;
    }

}
