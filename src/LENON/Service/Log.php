<?php

namespace LENON\Service;

use \DirectoryIterator,
    \Exception,
    \Zend\Filter\Compress,
    \Zend\Log\Logger,
    \Zend\Log\Writer\Stream;

class Log extends Logger
{

    private function comprimeArquivos($name, $path)
    {
        chdir($path);


        foreach (new DirectoryIterator($path) as $fileInfo) {
            if ($fileInfo->getExtension() == 'log' && ($fileInfo->getBasename() != $name)) {

                $rg = '/^\d{2}_\d{2}_\d{4}/';
                if (preg_match($rg, $fileInfo->getBasename())) {
                    $filter = new Compress(array(
                        'adapter' => 'Bz2',
                        'options' => array(
                            'archive' => "{$fileInfo->getBasename()}.bz2"
                        ),
                    ));
                    // troca o diretorio
                    $filter->filter("{$path}/{$fileInfo->getBasename()}");

                    // monta a base name
                    unlink("{$path}/{$fileInfo->getBasename()}");
                }
            }
        }
    }

    public function getFileName($path)
    {
        if (!file_exists($path)) {
            if (!mkdir($path, '0777', true)) {
                throw new Exception('Não foi possível criar o diretorio');
            }
        }
        elseif (!is_writeable($path)) {
            throw new Exception('Não foi escrever no diretorio');
        }

        $name = date('d_m_Y') . ".log";

        $fileName = "{$path}/{$name}";

        if (!file_exists($name)) {
            touch($fileName);
        }

        $this->comprimeArquivos($name, $path);

        return $fileName;
    }

    public function __construct($path, $options = null)
    {
        parent::__construct($options);
        if (!empty($path)) {
            $filename = $this->getFileName($path);
        }
        else {
            $filename = 'php://output';
        }
        $writer2 = new Stream($filename);

        $this->addWriter($writer2);
    }

    public function log($priority, $message, $extra = array())
    {
        $caller = debug_backtrace();

        return parent::log($priority,
                           "{$message}[{$caller['file']}:{$caller['line']}]",
                           $extra);
    }

}
