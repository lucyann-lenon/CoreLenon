<?php

namespace LENON\Model;

use Exception;
use Zend\Mail;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use LENON\Utils\SmtpOptions;

class EnviaMail extends Mail\Message
{

    /**
     *
     * @var \Zend\Mime\Message 
     */
    private $mimeMensagem = null;
    private $file         = null;

    public function __construct($file)
    {
        if (empty($file)) {
            $file = __DIR__ . '/../../../../../config/autoload/mail.global.php';
        }
        $this->file = $file;


        $this->mimeMensagem = new MimeMessage();
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    public function getConfig($rand)
    {
        if (!file_exists($this->file)) {
            throw new Exception('Nenhuma configuração para email encontrada !');
        }


        $config = include $this->file;

        if ($rand) {
            return $config['rand'][rand(0, count($config['rand']))];
        }
        else {
            return $config['config'];
        }
    }

    public function send($rand = FALSE)
    {
        $this->setBody($this->mimeMensagem);

        $config  = $this->getConfig($rand);
        // $options = new SmtpOptions($config);
        $options = new SmtpOptions($config);
        $options->setAllowSelfSigned();

        // add from 
        $this->addFrom($config['connection_config']['username'], $config['connection_config']['name']);

        $transport = new SmtpTransport();
        $transport->setOptions($options);

        $transport->send($this);
    }

    public function addMessage($content, $type = 'text/html')
    {
        $part       = new MimePart($content);
        $part->type = $type;

        $this->mimeMensagem->addPart($part);
    }
    

}
