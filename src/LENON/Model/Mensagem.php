<?php

namespace LENON\Model;

use Zend\Session\Container;

class Mensagem
{

    const ALERT    = 0;
    const INFO     = 1;
    const SUCCESS  = 2;
    const ERROR    = 3;
    const BLOCK    = 4;
    const QUESTION = 4;

    protected static $tipoMensagem      = array(
        self::ALERT   => 'alert',
        self::INFO    => 'alert alert-info',
        self::SUCCESS => 'alert alert-success',
        self::ERROR   => 'alert alert-danger',
        self::BLOCK   => 'alert alert-block',
    );
    protected static $tipoExtjsMensagem = array(
        self::ALERT    => 'Ext.window.MessageBox.WARNING',
        self::INFO     => 'Ext.window.MessageBox.INFO',
        self::SUCCESS  => 'Ext.window.MessageBox.INFO',
        self::ERROR    => 'Ext.window.MessageBox.ERROR',
        self::QUESTION => 'Ext.window.MessageBox.QUESTION'
    );

    /**
     * 
     * @param type $tipo
     * @param type $mensagem
     * @param type $log
     * @param type $flash
     * @param array $options array(
     * 'modal'=>true/false default=false,
     * 'button'=>botoes footer modal default close
     * )
     */
    public static function setMensagem($tipo, $mensagem, $log = true, $flash = 1, array $options = array())
    {
        // inicia uma sessao para a mensagem 
        $conteiner            = new Container('Mensagem');
        $mensagens            = json_decode($conteiner->mensagens, true);
        $mensagens[]          = array('mensagem' => $mensagem,
            'tipo'     => $tipo,
            'flash'    => $flash,
            'options'  => $options
        );
        $conteiner->mensagens = json_encode($mensagens);
    }

    public static function setExtjsMensagem($tipo, $mensagem, $log = true, $flash = 1, array $options = array())
    {
        // inicia uma sessao para a mensagem 
        $conteiner            = new Container('Mensagem');
        $mensagens            = json_decode($conteiner->mensagens, true);
        $mensagens[]          = array('mensagem' => $mensagem,
            'tipoMsg'  => 'extjs',
            'tipo'     => $tipo,
            'flash'    => $flash,
            'options'  => $options
        );
        $conteiner->mensagens = json_encode($mensagens);
    }

    public static function getMensage()
    {
        $conteiner = new Container('Mensagem');

        $mensagens       = json_decode($conteiner->mensagens, true);
        $mensagensRender = "";

        // monta as linhas 
        foreach ($mensagens as $k => $row) {
            // aumenta a flash atual
            $row['atualFlash'] ++;

            // verifica o flash
            if ($row['flash'] == $row['atualFlash']) {
                // limpa a variavel
                unset($mensagens[$k]);
                if ($row['tipoMsg'] === 'extjs') {

                    $optionsExtjs = json_encode(array(
                        'title'   => 'Atenção',
                        'msg'     => $row['mensagem'],
                        'estatus' => self::$tipoExtjsMensagem[$row['tipo']],
                        'options' => $row['options']
                    ));

                    $mensagensRender .= "<script type='text/javascript'>msgExtJs($optionsExtjs)</script>";
                }
                else {
                    if ($row['options']['modal']) {
                        $id = "a" . md5(uniqid());

                        $mensagensRender .='<div class="modal fade" id="' . $id . '">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                ';
                        if (empty($row['options']['button'])) {
                            $mensagensRender .= ' <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
                        }

                        $mensagensRender.='<h4 class="modal-title">Atenção</h4>
                                </div>
                                <div class="modal-body">
                                  <p class="' . $row['options']['p-class'] . '">' . $row['mensagem'] . '</p>
                                </div>
                                <div class="modal-footer">
                                ';
                        if (!empty($row['options']['button'])) {
                            $mensagensRender .= $row['options']['button'];
                        }
                        else {
                            $mensagensRender .= '<button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>';
                        }
                        $mensagensRender.=' </div>
                              </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          </div><!-- /.modal -->
                          <script type="text/javascript">$(\'#' . $id . '\').modal(\'show\');</script>';
                    }
                    else {

                        $mensagensRender .= "<div class=\"" . self::$tipoMensagem[$row['tipo']] . "\">"
                                . "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" onClick=\"javascript:closeMsg(this)\">×</button>"
                                . $row['mensagem']
                                . "</div>";
                    }
                }
            }
            else {
                $mensagens[$k] = $row;
            }
        }


        /*
          <div class="alert alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <h4>Advertência!</h4>
          Melhor checar você mesmo, você não está...
          </div>
         *          */
        $conteiner->mensagens = json_encode($mensagens);
        // \Zend\Debug\Debug::dump($mensagensRender);

        return $mensagensRender;
    }

}
