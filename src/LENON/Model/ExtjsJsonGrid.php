<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LENON\Model;

use Exception;

/**
 * Description of DoctrineExtjsGrid
 *
 * @author lucyann
 */
class ExtjsJsonGrid
{

    /**
     *
     * @var boll
     */
    protected $useCache = false;

    /**
     *
     * @var string (sql=>1,dql=>2,doctrineQueryBilder=>3)
     */
    protected $tipo = '3';

    /**
     *
     * @var \Doctrine\DBAL\Query\QueryBuilder 
     * 
     */
    protected $sql = '';

    /**
     *
     * @var \Doctrine\DBAL\Query\QueryBuilder 
     * 
     */
    protected $sqlCount = '';

    /**
     *
     * @var \Zend\Cache\Storage\Adapter\Memcached 
     */
    protected $cache = null;

    /**
     *
     * @var array 
     */
    protected $execution = array();

    /**
     *
     * @var \Doctrine\ORM\EntityMananger 
     */
    protected $em = null;

    /**
     *
     * @var array 
     */
    protected $order = array();

    /**
     *
     * @var int
     */
    protected $limit = null;

    /**
     *
     * @var int
     */
    protected $offset = null;

    /**
     *
     * @var array
     */
    protected $rowsSql;

    /**
     *
     * @var int
     */
    protected $totalCount;

    public function __construct($em, $cache = null)
    {
        if (!($em instanceof \Doctrine\ORM\EntityManager)) {
            throw new Exception('A classe precisa de um \Doctrine\ORM\EntityManager => $em');
        }
        $this->em = $em;
    }

    public function getUseCache()
    {
        return $this->useCache;
    }

    public function getUseDql()
    {
        return $this->useDql;
    }

    public function getSql()
    {
        return $this->sql;
    }

    public function getExecution()
    {
        return $this->execution;
    }

    public function setUseCache($useCache)
    {
        $this->useCache = $useCache;
        return $this;
    }

    public function setUseDql($useDql)
    {
        $this->useDql = $useDql;
        return $this;
    }

    public function setSql($sql)
    {
        $this->sql = $sql;
        return $this;
    }

    public function setSqlQueryBilder($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    public function setExecution($execution)
    {
        $this->execution = $execution;
        return $this;
    }

    public function getSqlCount()
    {
        return $this->sqlCount;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setSqlCount($sqlCount)
    {
        $this->sqlCount = $sqlCount;
        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function getCache()
    {
        return $this->cache;
    }

    public function getEm()
    {
        return $this->em;
    }

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function setCache(\Zend\Cache\Storage\Adapter\Memcached $cache)
    {
        $this->cache = $cache;
        return $this;
    }

    public function setEm(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
        return $this;
    }

    public function execute()
    {
        if ($this->useCache) {
            if (!is_object($this->cache)) {
                throw new Exception('É necessário uma classe para gerenciar o sistema de Cache!');
            }
        }

        switch ($this->tipo) {
            case 3:
                $this->sqlQueryBilderExecute()
                ;
        }

        return $this;
    }

    private function mountSqlQueryBilder()
    {

        if (empty($this->sql)) {
            throw new Exception("É necessário ter o sql");
        }

        if (empty($this->sqlCount)) {
            throw new Exception("É necessário ter o sqlcount ");
        }

        // verifica se existe uma ordem 
        if (count($this->order) > 0) {
            foreach ($this->order as $rowOrder) {
                $this->sql->addOrderBy($rowOrder['colunm'], $rowOrder['order']);
            }
        }

        // verifica se existe limite 
        if ($this->limit > 0) {
            $this->sql->setMaxResults($this->limit);
        }

        // verica qual é o numero de registro 
        if ($this->offset > 0) {
            $this->sql->setFirstResult($this->offset);
        }

        // monta os sqls 
        if (count($this->execution) > 0) {
            $this->sql->setParameters($this->execution);
            $this->sqlCount->setParameters($this->execution);
        }
    }

    private function sqlQueryBilderExecute()
    {

        /**
         * montar o sql
         */
        $this->mountSqlQueryBilder();

        $this->rowsSql = $this->sql->execute()->fetchAll();

        if ($this->useCache) {
            if (!$this->totalCount = $this->cache->getItem(md5($this->sqlCount->getSQL()))) {

                $this->totalCount = $this->sql->execute()->fetchColumn(0);

                $this->cache->addItem(md5($this->sqlCount), $this->totalCount);
            }
        }
        else {
            $this->totalCount = $this->sqlCount->execute()->fetchColumn(0);
            ;
        }
    }

    /**
     * 
     * @param type $where
     * @param array $paran
     * @return \LENON\Model\ExtjsJsonGrid
     */
    public function addWhere($where, array $parans = array())
    {
        $this->sql->where($where);

        $this->sqlCount->where($where);
        foreach ($parans as $k => $paran):
            $this->execution[$k] = $paran;
        endforeach;


        return $this;
    }

    public function addOrder(array $array = array())
    {
        $this->order[] = $array;
    }

    /**
     * @return array
     */
    public function getResult()
    {

        return array(
            "total" => $this->totalCount,
            "data"  => $this->rowsSql
        );
    }

    public function json($response)
    {
        // monta o json 
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        $response->setContent(json_encode($this->getResult()));
        return $response;
    }

    public function montarOrderExtjs($sort)
    {
        // monta o route 
        $sort   = json_decode($sort, true);
        $sortBy = array();
        // monta o sort 
        foreach ($sort as $rowSort) {
            $sortBy[] = array(
                'colunm' => $rowSort['property'],
                'order'  => $rowSort['direction']
            );
        }


        return $sortBy;
    }

}
