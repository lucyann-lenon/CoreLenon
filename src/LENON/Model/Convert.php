<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConvertToByteSize
 *
 * @author lucyann
 */

namespace LENON\Model;

class Convert
{

    public static function toByteSize($tamanho)
    {


        // unidades de converção 
        $unidades = array('B' => 0, 'K' => 1, 'M' => 2, 'G' => 3);

        // pega a unidade de medida
        $unidade = substr($tamanho, -1);

        $tamanho = str_replace(array_keys($unidades), '', $tamanho);

        if (is_numeric($unidade)) {
            $unidade = 'B';
        }


        return $tamanho * pow(1024, $unidades[strtoupper($unidade)]);
    }

    public static function segundosToFormatHMS($seconds)
    {
        $hours   = floor($seconds / 3600);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $seconds -= ($minutes * 60);
        $seconds = (int) $seconds;
        return "$hours : $minutes : $seconds";
    }

    public static function responseToJson($response, array $data = array())
    {
        // monta o json 
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        $response->setContent(json_encode($data));
        return $response;
    }

}
