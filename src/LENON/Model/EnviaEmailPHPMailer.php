<?php

namespace LENON\Model;

use PHPMailer;

class EnviaEmailPHPMailer
{

    /**
     *
     * @var \PHPMailer 
     */
    private $mail;

    public function __construct($file)
    {
        if (empty($file)) {
            $file = __DIR__ . '/../../../../../config/autoload/mail.global.php';
        }
        $this->file = $file;

        $this->mail = new PHPMailer;
        $this->mail->isSMTP();
    }

    public function getConfig($rand)
    {
        if (!file_exists($this->file)) {
            throw new Exception('Nenhuma configuração para email encontrada !');
        }


        $config = include $this->file;

        if ($rand) {
            return $config['rand'][rand(0, count($config['rand']))];
        }
        else {
            return $config['config'];
        }
    }

    private function config($rand)
    {
        $config                 = $this->getConfig($rand);
        // Enable verbose debug output
        // Set mailer to use SMTP
        $this->mail->Host       = $config['host']; // Specify main and backup SMTP servers
        $this->mail->SMTPAuth   = true;                               // Enable SMTP authentication
        $this->mail->Username   = $config["connection_config"]["username"];
        $this->mail->Password   = $config["connection_config"]["password"];
        $this->mail->SMTPSecure = $config["connection_config"]["ssl"];                            // Enable TLS encryption, `ssl` also accepted
        $this->mail->Port       = $config['port'];
        $this->mail->setFrom($config["connection_config"]["username"], $config["connection_config"]["name"]);

        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true
            )
        );
// TCP port to connect to
    }

    public function addMsgHtml($msg)
    {
        $this->mail->isHTML(true);                                  // Set email format to HTML
        $this->mail->Body = $msg;
    }

    public function msgAlternativa($msg)
    {

        $this->mail->AltBody = $msg;
    }

    public function addTo($mail, $nome = null)
    {
        $this->mail->addAddress($mail, $nome);
        return $this;
    }

    public function setAssunto($assunto)
    {
        $this->mail->Subject = $assunto;
        return $this;
    }

    public function send($rand = false)
    {
        // configura email 
        $this->config($rand);



        if (!$this->mail->send()) {

            throw new \Exception('Mailer Error: ' . $this->mail->ErrorInfo);
        }
        else {
            return true;
        }
    }

}
