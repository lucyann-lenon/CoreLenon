<?php

namespace LENON\Model;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

class LogFile extends Logger
{

    public static $file = '/var/log/sistema.log';

    //put your code here
    public function __construct($tipo = Null, $msg = null, $file = null, $options = array())
    {
        parent::__construct($options);

        if (empty($file)) {
            $file = self::$file;
        }
        if (!empty($tipo) && !empty($msg)) {
            $writer = new Stream($file);
            $this->addWriter($writer);
            $this->log($tipo, $msg);
        }
    }

    private function getFile($file)
    {
        if (empty($file)) {
            $file = self::$file;
        }
        return $file;
    }

    private function getMsg($msg, $line)
    {
        if (!empty($line)) {
            return "{$msg} (line:{$line})";
        }
        else {
            return $msg;
        }
    }

    public function logInfo($msg, $line = null, $file = null)
    {

        if (!empty($msg)) {
            $file = $this->getFile($file);

            $writer = new Stream($file);
            $this->addWriter($writer);
            $this->log(self::INFO, $this->getMsg($msg, $line));
        }
    }

    public function logError($msg, $line = null, $file = null)
    {

        if (!empty($msg)) {
            $file = $this->getFile($file);

            $writer = new Stream($file);
            $this->addWriter($writer);
            $this->log(self::ERR, $this->getMsg($msg, $line));
        }
    }

    public function logNotice($msg, $line = null, $file = null)
    {

        if (!empty($msg)) {
            $file = $this->getFile($file);

            $writer = new Stream($file);
            $this->addWriter($writer);
            $this->log(self::NOTICE, $this->getMsg($msg, $line));
        }
    }

}
