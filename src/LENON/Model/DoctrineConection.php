<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Doc
 *
 * @author lucyann
 */

namespace LENON\Model;

use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

class DoctrineConection
{

    //put your code here
    private static $em;

    public static function resetEm()
    {
        self::$em = null;
    }

    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public static function getEm()
    {
        try {
            if (!(self::$em instanceof EntityManager)) {
                $filename = __DIR__ . '/../../../../../config/autoload/doctrine.local.php';

                if (!file_exists($filename)) {
                    $filename = __DIR__ . '/../../../../../config/autoload/doctrine.global.php';
                }

                $config = include $filename;

                $doctrineConfig = new Configuration();
                $cache          = new $config['doctrine']['driver']['cache'];
                $doctrineConfig->setQueryCacheImpl($cache);
                $doctrineConfig->setProxyDir($config['doctrine']['configuration']['orm_default']['proxy_dir']);
                $doctrineConfig->setProxyNamespace($config['doctrine']['configuration']['orm_default']['proxy_namespace']);
                $doctrineConfig->setAutoGenerateProxyClasses(true);
                $driver         = new AnnotationDriver(
                        new AnnotationReader(), array($config['doctrine']['driver']['application_entities']['paths'])
                );
                $doctrineConfig->setMetadataDriverImpl($driver);
                $doctrineConfig->setMetadataCacheImpl($cache);
                AnnotationRegistry::registerFile(__DIR__ .
                        '/../../../../../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
                );

                $options           = $config['doctrine']['connection']['orm_default']['params'];
                $options['driver'] = 'pdo_mysql';

                // cria o gerenciador de entidades 
                $em = EntityManager::create($options, $doctrineConfig);
                self::$em = $em;
            }
            else {
                $em = self::$em;
            }
            return $em;
        }
        catch (\Exception $e) {
            new LogFile(LogFile::CRIT, $e->getMessage());
        }
    }

}
