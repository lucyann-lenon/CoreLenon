<?php

namespace LENON\Model;


use Zend\Session\Container;
class IncludeJs {

    //put your code here

    public static function setRouteName($routeReal)
    {
        // inicia uma sessao para a mensagem 
        $conteiner            = new Container('Route');
        $conteiner->routeReal = strtolower($routeReal);
    }

    public static function getJs()
    {
        // inicia uma sessao para a mensagem 
        $conteiner            = new Container('Route');
        $caminho = $conteiner->routeReal ;
      //   \Zend\Debug\Debug::dump($caminho);
       if(file_exists("{$_SERVER["DOCUMENT_ROOT"]}/js/App/{$caminho}.min.js"))
       {
           return "<script src=\"/js/App/{$caminho}.min.js\" type=\"text/javascript\"></script>";
       }else
       {
           return '';
       }
        
    }

}
