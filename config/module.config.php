<?php

return array(
    'router'             => array(
        'routes' => array(
            'Forbidden'  => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/forbidden',
                    'defaults' => array(
                        '__NAMESPACE__' => 'LENON\Controller',
                        'controller'    => 'Index',
                        'action'        => 'forbidden',
                        'module'        => 'lenon'
                    )
                )
            ),
            'naoLogado'  => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/nao-logado',
                    'defaults' => array(
                        '__NAMESPACE__' => 'LENON\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'lenon'
                    )
                )
            ),
            'noresource' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/no-resource',
                    'defaults' => array(
                        '__NAMESPACE__' => 'LENON\Controller',
                        'controller'    => 'Index',
                        'action'        => 'no-resource',
                        'module'        => 'lenon'
                    )
                )
            ),
            'Captcha'    => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/captcha[/id/:status]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'LENON\Controller',
                        'controller'    => 'Index',
                        'action'        => 'captcha',
                        'module'        => 'lenon'
                    )
                )
            )
        )
    ),
    'controllers'        => array(
        'invokables' => array(
            'LENON\Controller\Index' => 'LENON\Controller\IndexController'
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'AclPlugin' => 'LENON\Controller\Plugin\AclPlugin',
        )
    ),
    'view_manager'       => array(
        'template_path_stack' => array(
            'lenon' => __DIR__ . '/../view',
        ),
    ),
);
