<?php

namespace LENON;

use Zend\Mvc\MvcEvent,
    Zend\I18n\Translator\Translator,
    Zend\Mvc\I18n\Translator as Translator2,
    Zend\Validator\AbstractValidator,
    Zend\ModuleManager\Feature\AutoloaderProviderInterface,
    Zend\ModuleManager\Feature\ConfigProviderInterface;
use \LENON\Model\LogFile;

class Module implements
AutoloaderProviderInterface, ConfigProviderInterface
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach('route',
                              array(
            $this,
            'loadConfiguration'), 2);
        //you can attach other function need here...
        try {
            $translator = new Translator();

            $filename = __DIR__ . "/../../vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Validate.php";

            if (!file_exists($filename)) {
                $filename = __DIR__ . '/../../vendor/zendframework/zend-i18n-resources/languages/pt_BR/Zend_Validate.php';
            }

            $translator->addTranslationFile('phpArray', $filename, 'default',
                                            'pt_BR');

            $translatorMvc = new Translator2($translator);

            AbstractValidator::setDefaultTranslator($translatorMvc);
        }
        catch (\Exception $e) {
            echo $e->getMessage();
            exit;
        }

        $arquivo = include __DIR__ . '/../../config/autoload/global.php';

        // seta o local do file 
        \LENON\Model\LogFile::$file = $arquivo['logFile'];

        $sharedManager = $e->getApplication()->getEventManager()->getSharedManager();
        $sm            = $e->getApplication()->getServiceManager();
        $sharedManager->attach('Zend\Mvc\Application', 'dispatch.error',
                               function($e) use ($sm) {
            if ($e->getParam('exception')) {
                $sm->get('Log')->crit($e->getParam('exception'));
            }
        }
        );
    }

    public function loadConfiguration(MvcEvent $e)
    {
        $application   = $e->getApplication();
        $sm            = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $router  = $sm->get('router');
        $request = $sm->get('request');

        $matchedRoute = $router->match($request);
        if (null !== $matchedRoute && !$sm->get('config')['pararAcl']) {
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController',
                                   'dispatch',
                                   function($e) use ($sm) {
                $sm->get('ControllerPluginManager')->get('AclPlugin')
                        ->doAuthorization($e); //pass to the plugin...    
            }, 2);
        }
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Cache' => function($sm) {
                    // diretorio do aquivo 
                    $filename = __DIR__ . '/../../config/cache.config.php';

                    // verifica se o arquivo existe 
                    if (!file_exists($filename)) {
                        throwException('O arquivo de confituração do cache não existe');
                    }

                    // cria o cache 
                    $cfg   = include $filename;
                    $cache = \Zend\Cache\StorageFactory::factory($cfg);

                    return $cache;
                },
                'log' => function($sm) {
                    return new Service\Log($sm->get('config')['logPath']);
                }
            )
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(
                'formelementerrors' => 'LENON\Form\View\Helper\FormElementErrors'
            ),
        );
    }

}
